// Fill out your copyright notice in the Description page of Project Settings.


#include "FlapFlap.h"
#include "MainMenuHUD.h"
#include "SMainMenuWidget.h"

void AMainMenuHUD::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	
	SAssignNew(MainMenuUI, SMainMenuWidget).MainMenuHUD(this);

	if(GEngine->IsValidLowLevel())
	{
		GEngine->GameViewport->AddViewportWidgetContent(SNew(SWeakWidget).PossiblyNullContent(MainMenuUI.ToSharedRef()));
	}
}
