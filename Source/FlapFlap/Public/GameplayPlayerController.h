// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameplayHUD.h"
#include "SavePlayer.h"
#include "Gamester.h"
#include "FieldActor.h"
#include "FieldGame.h"
#include "GameFramework/PlayerController.h"
#include "GameplayPlayerController.generated.h"

/**
 *
 */
UCLASS()
class FLAPFLAP_API AGameplayPlayerController : public APlayerController
{
	GENERATED_BODY()

		void BeginPlay()override;
public:
	void Turn();

	void RestartGame();

	void OpenMainMenu();

	FString GetWinner()const;
	
	FString GetScore()const;
private:
	UPROPERTY()
		APlayerController* PlayerController = nullptr;

	UPROPERTY()
		TArray<AFieldActor*> FieldActors;

	UPROPERTY()
		USavePlayer* SavePlayers[2];
	
	UFUNCTION()
		FVector GetMousePositionOnWorld();

	UFUNCTION()
		void ChangePlayer();

	TSharedPtr<Gamester> Players[2];

	TWeakPtr<Gamester> CurrentPlayer;

	AGameplayHUD* GameplayHUD;

	int countTurn;

	FieldGame* fieldsGame[3][3];

	bool IsOverRound = false;
	
	void CreateNewGame(UWorld* World);

	void CreatePlayers();

	void FindFields();

	void SetCamera();

	void SpawnFields(UWorld* World);

	void SaveGame();

	void LoadGame();
	
	bool CheckWin();

	bool CheckWinPerpendicularly();

	bool CheckWinHorizontally();

	bool CheckWinAslant();
};