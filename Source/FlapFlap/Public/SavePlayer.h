// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Gamester.h"
#include "GameFramework/SaveGame.h"
#include "SavePlayer.generated.h"

/**
 *
 */
UCLASS()
class FLAPFLAP_API USavePlayer : public USaveGame
{
public:
	GENERATED_BODY()

		UPROPERTY(VisibleAnywhere, Category = Basic)
		FString SaveSlotName;

	UPROPERTY(VisibleAnywhere, Category = Basic)
		uint32 UserIndex;

	UPROPERTY(VisibleAnywhere, Category = Player)
		FString PlayerName;

	UPROPERTY(VisibleAnywhere, Category = Player)
		uint32 score;

	UPROPERTY(VisibleAnywhere, Category = Player)
		EPlayerSymbol symbol;

	USavePlayer();
};
