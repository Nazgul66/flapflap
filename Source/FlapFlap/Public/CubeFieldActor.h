// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "FieldGame.h"
#include "GameFramework/Actor.h"
#include "CubeFieldActor.generated.h"

UCLASS()
class FLAPFLAP_API ACubeFieldActor : public AActor, public FieldGame
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACubeFieldActor();

	virtual void Destroy()override;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UBoxComponent* BoxComponent;

	UStaticMeshComponent* MeshComponent;
};
