// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "MainMenuPlayerController.generated.h"

/**
 *
 */
UCLASS()
class FLAPFLAP_API AMainMenuPlayerController: public APlayerController
{
	GENERATED_BODY()

public:
	void BeginPlay()override;


	UPROPERTY()
		APlayerController* PlayerController;
};
