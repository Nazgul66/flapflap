// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "SlateBasics.h"
#include "GameplayHUD.h"
#include "GameplayPlayerController.h"
#include "Widgets/SCompoundWidget.h"
/**
 *
 */
class FLAPFLAP_API SGameplayWidget : public SCompoundWidget
{
public:
	TWeakObjectPtr<AGameplayHUD> GameplayHUD;

	SLATE_BEGIN_ARGS(SGameplayWidget)
	{}
	SLATE_ARGUMENT(TWeakObjectPtr<AGameplayHUD>, GameplayHUD)
		SLATE_END_ARGS()

		/** Constructs this widget with InArgs */
		void Construct(const FArguments& InArgs);

	FReply ButtonYes();
	FReply ButtonNo();

	//FText GetCurrentPlayerTurn()const;
	FText GetGameResult()const;
};
