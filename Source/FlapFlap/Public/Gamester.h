// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EPlayerSymbol : uint8
{
	E_CIRCLE 	UMETA(DisplayName = "Circle"),
	E_CROSS		UMETA(DisplayName = "Cross")
};

class FLAPFLAP_API Gamester
{
public:
	Gamester(FString Name, EPlayerSymbol Symbol, int Score = 0);
	~Gamester();

	void AdmitWin();

	EPlayerSymbol GetSymbol()const;

	FString GetPlayerName()const;

	int GetScore()const;
private:
	EPlayerSymbol Type;

	int Score;

	FString Name;
};
