// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameFramework/HUD.h"
#include "GameplayHUD.generated.h"

/**
 * 
 */
UCLASS()
class FLAPFLAP_API AGameplayHUD : public AHUD
{
	GENERATED_BODY()
	
		virtual void PostInitializeComponents() override;

	TSharedPtr<class SGameplayWidget> GameplayUI;
	
public:
	void HideWidget();

	void ShowWidget();
};
