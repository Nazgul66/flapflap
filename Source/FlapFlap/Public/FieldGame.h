// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Gamester.h"

/**
 * 
 */
class FLAPFLAP_API FieldGame
{
public:
	FieldGame();
	FieldGame(EPlayerSymbol symbol, int x, int y);
	~FieldGame();

	virtual void Destroy() = 0;

	UFUNCTION()
	int GetX()const;

	UFUNCTION()
	int GetY()const;

	UFUNCTION()
	EPlayerSymbol GetSymbol()const;


	UFUNCTION()
	void SetIndexField(uint16_t x, uint16_t y);
protected:
	int x = -1;
	int y = -1;

	EPlayerSymbol symbol;
};
