// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "GameplayGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class FLAPFLAP_API AGameplayGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
	public:
		AGameplayGameModeBase();
	
};
