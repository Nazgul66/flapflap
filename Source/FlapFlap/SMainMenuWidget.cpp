// Fill out your copyright notice in the Description page of Project Settings.

#include "FlapFlap.h"
#include "SMainMenuWidget.h"
#include "SlateOptMacros.h"
#include "MainMenuPlayerController.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SMainMenuWidget::Construct(const FArguments& InArgs)
{
	MainMenuHUD = InArgs._MainMenuHUD;

	ChildSlot
		[
			SNew(SOverlay)
			+ SOverlay::Slot().HAlign(HAlign_Center).VAlign(VAlign_Top)
		[
			SNew(STextBlock).ColorAndOpacity(FLinearColor::Blue)
			.ShadowColorAndOpacity(FLinearColor::Black)
		.ShadowOffset(FIntPoint(-2, 2))
		.Font(FSlateFontInfo("Arial", 60))
		.Text(FText::FromString("Lukasz MLynarski - Flop Flop"))
		]
	+ SOverlay::Slot().HAlign(HAlign_Center).VAlign(VAlign_Center)
		[

			SNew(SVerticalBox) + SVerticalBox::Slot()
			[
				//Button play
				SNew(SButton)
				.OnClicked(this, &SMainMenuWidget::Play)
		.DesiredSizeScale(FVector2D(1.2, 2.5))
		[
			SNew(SOverlay)
			+ SOverlay::Slot().HAlign(HAlign_Center).VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Font(FSlateFontInfo("Arial", 40))
		.Text(FText::FromString("Play!"))
		]
		]
			]

	+ SVerticalBox::Slot()
		[
			SNew(SButton)
			.OnClicked(this, &SMainMenuWidget::QuitGame)
		[
			SNew(SOverlay)
			+ SOverlay::Slot().HAlign(HAlign_Center).VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Font(FSlateFontInfo("Arial", 40))
		.Text(FText::FromString("Quit!"))
		]
		]
		]
		]
		];
}

END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply SMainMenuWidget::Play()
{
	for(TObjectIterator<AMainMenuPlayerController> iterator; iterator; ++iterator)
	{
		if(GEngine->IsValidLowLevel())
		{
			UWorld* World = (*iterator)->GetWorld();

			if(World)
			{
				UGameplayStatics::OpenLevel(World, "Gameplay");
				break;
			}
		}
	}

	return FReply::Handled();
}


FReply SMainMenuWidget::QuitGame()
{
	FGenericPlatformMisc::RequestExit(false);
	return FReply::Handled();
}