// Fill out your copyright notice in the Description page of Project Settings.

#include "FlapFlap.h"
#include "SGameplayWidget.h"
#include "SlateOptMacros.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SGameplayWidget::Construct(const FArguments& InArgs)
{
	GameplayHUD = InArgs._GameplayHUD;

	ChildSlot
		[
			SNew(SOverlay)
			+ SOverlay::Slot().HAlign(HAlign_Left).VAlign(VAlign_Top)
		[
			SNew(STextBlock).ColorAndOpacity(FLinearColor::Blue)
			.ShadowColorAndOpacity(FLinearColor::Black)
		.ShadowOffset(FIntPoint(-2, 2))
		.Font(FSlateFontInfo("Arial", 60))
		.Text(this, &SGameplayWidget::GetGameResult)
		]
	+ SOverlay::Slot().HAlign(HAlign_Left).VAlign(VAlign_Center)
		[
			SNew(SVerticalBox) + SVerticalBox::Slot()
			[
				SNew(STextBlock).ColorAndOpacity(FLinearColor::Blue)
				.ShadowColorAndOpacity(FLinearColor::Black)
		.ShadowOffset(FIntPoint(-2, 2))
		.Font(FSlateFontInfo("Arial", 60))
		.Text(FText::FromString("Play again?"))
			] + SVerticalBox::Slot()
		[
			SNew(SOverlay)
			+ SOverlay::Slot().HAlign(HAlign_Left).VAlign(VAlign_Top)
		[
			SNew(SButton)
			.OnClicked(this, &SGameplayWidget::ButtonYes)
		.DesiredSizeScale(FVector2D(1.2, 2.0))
		[
			SNew(SOverlay)
			+ SOverlay::Slot().HAlign(HAlign_Center).VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Font(FSlateFontInfo("Arial", 40))
		.Text(FText::FromString("Yes!"))
		]
		]
		]
		]
	+ SVerticalBox::Slot()

		[
			SNew(SButton)
			.OnClicked(this, &SGameplayWidget::ButtonNo)
		.DesiredSizeScale(FVector2D(1.2, 2.0))
		[
			SNew(SOverlay)
			+ SOverlay::Slot().HAlign(HAlign_Center).VAlign(VAlign_Bottom)
		[
			SNew(STextBlock)
			.Font(FSlateFontInfo("Arial", 40))
		.Text(FText::FromString("No!"))

		]
		]
		]
		]
		];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION


FReply SGameplayWidget::ButtonYes()
{
	AGameplayPlayerController* PlayerController = nullptr;
	if (!PlayerController)
	{
		for (TObjectIterator<AGameplayPlayerController> PC; PC; ++PC)
		{
			PlayerController = *PC;
			break;
		}
	}

	if (PlayerController)
		PlayerController->RestartGame();

	return FReply::Handled();
}

FReply SGameplayWidget::ButtonNo()
{
	AGameplayPlayerController* PlayerController = nullptr;
	if (!PlayerController)
	{
		for (TObjectIterator<AGameplayPlayerController> PC; PC; ++PC)
		{
			PlayerController = *PC;
			break;
		}
	}

	PlayerController->OpenMainMenu();

	return FReply::Handled();
}

//FText SGameplayWidget::GetCurrentPlayerTurn()const
//{
//	return FText::FromString("LOL");
//}

FText SGameplayWidget::GetGameResult()const
{
	AGameplayPlayerController* PlayerController = nullptr;

	for (TObjectIterator<AGameplayPlayerController> PC; PC; ++PC)
		PlayerController = *PC;

	if (PlayerController)
		return FText::FromString("The winner is: " + PlayerController->GetWinner() + " score: " + PlayerController->GetScore());

	return FText();
}
