// Fill out your copyright notice in the Description page of Project Settings.

#include "FlapFlap.h"
#include "FieldGame.h"


FieldGame::FieldGame()
{
}

FieldGame::FieldGame(EPlayerSymbol symbol, int x, int y)
	:x(x), y(y), symbol(symbol)
{
}

FieldGame::~FieldGame()
{
}

int FieldGame::GetX()const {
	return x;
}

int FieldGame::GetY()const {
	return y;
}

EPlayerSymbol FieldGame::GetSymbol()const {
	return symbol;
}


void FieldGame::SetIndexField(uint16_t x, uint16_t y)
{
	if (this->x == -1 && this->y == -1) {
		this->x = x;
		this->y = y;
	}
}