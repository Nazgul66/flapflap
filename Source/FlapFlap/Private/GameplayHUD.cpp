// Fill out your copyright notice in the Description page of Project Settings.

#include "FlapFlap.h"
#include "GameplayHUD.h"
#include "SGameplayWidget.h"


void AGameplayHUD::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	SAssignNew(GameplayUI, SGameplayWidget).GameplayHUD(this);

	//ShowWidget();
}

void AGameplayHUD::HideWidget()
{
	if (GEngine->IsValidLowLevel())
	{
		GEngine->GameViewport->RemoveViewportWidgetContent(SNew(SWeakWidget).PossiblyNullContent(GameplayUI.ToSharedRef()));
	}
}

void AGameplayHUD::ShowWidget()
{
	if (GEngine->IsValidLowLevel())
	{
		GEngine->GameViewport->AddViewportWidgetContent(SNew(SWeakWidget).PossiblyNullContent(GameplayUI.ToSharedRef()));
	}	
}
