// Fill out your copyright notice in the Description page of Project Settings.

#include "FlapFlap.h"
#include "GameplayPlayerController.h"
#include "FieldActor.h"
#include "Kismet/GameplayStatics.h"
void AGameplayPlayerController::BeginPlay()
{
	UWorld* World = GetWorld();

	SavePlayers[0] = Cast<USavePlayer>(UGameplayStatics::CreateSaveGameObject(USavePlayer::StaticClass()));
	SavePlayers[1] = Cast<USavePlayer>(UGameplayStatics::CreateSaveGameObject(USavePlayer::StaticClass()));

	if (World)
	{
		PlayerController = World->GetFirstPlayerController();

		//Set enable mouse event and set camera
		if (PlayerController)
		{
			SetCamera();
		}

		CreateNewGame(World);
	}

	LoadGame();
	CreatePlayers();

	for (TObjectIterator<AGameplayHUD> HUD; HUD; ++HUD)
	{
		GameplayHUD = *HUD;
		break;
	}
}

void AGameplayPlayerController::Turn()
{
	if (GEngine->IsValidLowLevel()) 
	{
		if (!IsOverRound) 
		{
			FVector mousePosion = GetMousePositionOnWorld();

			for (AFieldActor* field : FieldActors)
			{
				if (field->CheckMouseHit(mousePosion))
				{
					field->SpawnField(CurrentPlayer.Pin()->GetSymbol(), fieldsGame);

					if (CheckWin()) {
						IsOverRound = true;
						CurrentPlayer.Pin()->AdmitWin();
						SaveGame();
						GameplayHUD->ShowWidget();
					}

					ChangePlayer();
					break;
				}
			}
		}
	}
}

FVector AGameplayPlayerController::GetMousePositionOnWorld()
{
	//mouse position and direction on world
	FVector mousePosion, mouseDirection;

	//mouse position on screen
	float xScreen, yScreen;
	PlayerController->GetMousePosition(xScreen, yScreen);

	PlayerController->DeprojectScreenPositionToWorld(xScreen, yScreen, mousePosion, mouseDirection);
	return mousePosion;
}

void AGameplayPlayerController::ChangePlayer()
{
	CurrentPlayer = Players[++countTurn % 2];
}

void AGameplayPlayerController::CreateNewGame(UWorld* World)
{
	if (World)
	{
		SpawnFields(World);

		for (uint16 i = 0; i < 3; ++i) {
			for (uint16 j = 0; j < 3; ++j)
				fieldsGame[i][j] = nullptr;
		}
	}
}

void AGameplayPlayerController::CreatePlayers()
{
	if (Players[0].Get() == nullptr || Players[1].Get() == nullptr)
	{
		Players[0] = TSharedPtr<Gamester>(new Gamester("P1", EPlayerSymbol::E_CIRCLE));
		Players[1] = TSharedPtr<Gamester>(new Gamester("P2", EPlayerSymbol::E_CROSS));
	}

	countTurn = rand() % 2;
	CurrentPlayer = Players[countTurn];
}

void AGameplayPlayerController::FindFields()
{
	//Find all gameplay field
	for (TObjectIterator<AFieldActor> field; field; ++field)
	{
		//chceck duplicate actors in table
		bool isDuplicate = false;

		for (AFieldActor* isField : FieldActors) {
			if (isField == *field) {
				isDuplicate = true;
				break;
			}
		}

		//is not yet
		if (!isDuplicate)
			FieldActors.Add(*field);
	}
}

void AGameplayPlayerController::SetCamera()
{
	PlayerController->bShowMouseCursor = true;
	PlayerController->bEnableClickEvents = true;
	PlayerController->bEnableMouseOverEvents = true;

	for (TObjectIterator<ACameraActor> Iterator; Iterator; ++Iterator)
	{
		if ((*Iterator)->GetName() == FString("GameplayCamera"))
		{
			PlayerController->SetViewTargetWithBlend((*Iterator));
			break;
		}
	}
}

void AGameplayPlayerController::SpawnFields(UWorld* World)
{
	int x = 650;
	int row = 0;
	for (int z = 310; z > 50; z -= 110) {
		int column = 0;
		for (int y = 140; y < 400; y += 110) {

			FVector position = FVector(x, y, z);

			AFieldActor* field = (AFieldActor*)World->SpawnActor(AFieldActor::StaticClass(), &position);
			FString name = "Field" + FString::FromInt(row) + "x" + FString::FromInt(column);

			field->Rename(*name);
			FieldActors.Add(field);

			++column;
		}
		++row;
	}
}

void AGameplayPlayerController::SaveGame()
{
	if (!SavePlayers[0] || !SavePlayers[1])
	{
		SavePlayers[0] = Cast<USavePlayer>(UGameplayStatics::CreateSaveGameObject(USavePlayer::StaticClass()));
		SavePlayers[1] = Cast<USavePlayer>(UGameplayStatics::CreateSaveGameObject(USavePlayer::StaticClass()));
	}

	SavePlayers[0]->SaveSlotName = "SlotPlayer0";
	SavePlayers[0]->UserIndex = 0;
	SavePlayers[0]->PlayerName = Players[0]->GetPlayerName();
	SavePlayers[0]->symbol = Players[0]->GetSymbol();
	SavePlayers[0]->score = Players[0]->GetScore();

	UGameplayStatics::SaveGameToSlot(SavePlayers[0], SavePlayers[0]->SaveSlotName, SavePlayers[0]->UserIndex);

	SavePlayers[1]->SaveSlotName = "SlotPlayer1";
	SavePlayers[1]->UserIndex = 1;
	SavePlayers[1]->PlayerName = Players[1]->GetPlayerName();
	SavePlayers[1]->symbol = Players[1]->GetSymbol();
	SavePlayers[1]->score = Players[1]->GetScore();

	UGameplayStatics::SaveGameToSlot(SavePlayers[1], SavePlayers[1]->SaveSlotName, SavePlayers[1]->UserIndex);
}

void AGameplayPlayerController::LoadGame()
{
	SavePlayers[0] = Cast<USavePlayer>(UGameplayStatics::LoadGameFromSlot("SlotPlayer0", 0));
	if (SavePlayers[0] != nullptr) {
		FString name = SavePlayers[0]->PlayerName;
		int score = SavePlayers[0]->score;
		EPlayerSymbol symbol = SavePlayers[0]->symbol;

		Players[0] = TSharedPtr<Gamester>(new Gamester(name, symbol, score));
	}


	SavePlayers[1] = Cast<USavePlayer>(UGameplayStatics::LoadGameFromSlot("SlotPlayer1", 1));
	if (SavePlayers[1] != nullptr) {
		FString name = SavePlayers[1]->PlayerName;
		int score = SavePlayers[1]->score;
		EPlayerSymbol symbol = SavePlayers[1]->symbol;

		Players[1] = TSharedPtr<Gamester>(new Gamester(name, symbol, score));
	}
}

void AGameplayPlayerController::RestartGame()
{
	IsOverRound = false;
	RestartLevel();
	////clear actual board
	//for (uint16 i = 0; i < 2; ++i) {
	//	for (uint16 j = 0; j < 2; ++j)
	//	{
	//		if (fieldsGame[i][j] != nullptr)
	//			fieldsGame[i][j]->Destroy();
	//	}
	//}

	//FieldActors.Empty();

	//for (AFieldActor* field : FieldActors) 
	//{
	//	FieldActors.Remove(field);
	//	field->Destroy();
	//}	

	//CreateNewGame(GetWorld());
}

void AGameplayPlayerController::OpenMainMenu()
{
	UWorld* World = GetWorld();

	if (World)
		UGameplayStatics::OpenLevel(World, "MainMenu");
}

FString AGameplayPlayerController::GetWinner() const
{
	return CurrentPlayer.Pin()->GetPlayerName();
}

FString AGameplayPlayerController::GetScore() const
{
	return FString::FromInt(CurrentPlayer.Pin()->GetScore());
}

bool AGameplayPlayerController::CheckWin() {
	if (CheckWinAslant() || CheckWinHorizontally() || CheckWinPerpendicularly())
		return true;

	return false;
}

bool AGameplayPlayerController::CheckWinPerpendicularly()
{
	if (GEngine->IsValidLowLevel()) {
		for (uint16 i = 0; i < 3; ++i) {
			uint16 circle = 0;
			uint16 cross = 0;

			for (uint16 j = 0; j < 3; ++j) {
				if (fieldsGame[i][j] != nullptr) {
					((fieldsGame[i][j]->GetSymbol() == EPlayerSymbol::E_CIRCLE)) ? ++circle : ++cross;
				}
			}

			if (circle == 3 || cross == 3)	return true;
		}
	}
	return false;
}

bool AGameplayPlayerController::CheckWinHorizontally()
{
	for (uint16 j = 0; j < 3; ++j) {
		uint16 circle = 0;
		uint16 cross = 0;

		for (uint16 i = 0; i < 3; ++i) {
			if (fieldsGame[i][j] != nullptr) {
				((fieldsGame[i][j]->GetSymbol() == EPlayerSymbol::E_CIRCLE)) ? ++circle : ++cross;
			}
		}

		if (circle == 3 || cross == 3)	return true;
	}
	return false;
}

bool AGameplayPlayerController::CheckWinAslant()
{
	if (GEngine->IsValidLowLevel()) {
		uint16 circle = 0;
		uint16 cross = 0;

		for (uint16 i = 0; i < 3; ++i) {
			if (fieldsGame[i][i] != nullptr) {
				((fieldsGame[i][i]->GetSymbol() == EPlayerSymbol::E_CIRCLE)) ? ++circle : ++cross;
			}
		}

		if (circle == 3 || cross == 3)	return true;

		circle = 0;
		cross = 0;

		uint16 k = 2;
		for (uint16 j = 0; j < 3; ++j) {
			if (fieldsGame[k][j] != nullptr) {
				((fieldsGame[k][j]->GetSymbol() == EPlayerSymbol::E_CIRCLE)) ? ++circle : ++cross;
			}
			--k;
		}

		if (circle == 3 || cross == 3)	return true;

	}
	return false;
}
