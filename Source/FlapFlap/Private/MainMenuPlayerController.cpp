// Fill out your copyright notice in the Description page of Project Settings.

#include "FlapFlap.h"
#include "MainMenuPlayerController.h"


void AMainMenuPlayerController::BeginPlay()
{
	UWorld* World = GetWorld();

	if(World)
	{
		PlayerController = World->GetFirstPlayerController();

		if(PlayerController)
		{
			PlayerController->bShowMouseCursor = true;
			PlayerController->bEnableClickEvents = true;
			PlayerController->bEnableMouseOverEvents = true;
		}
	}
}

