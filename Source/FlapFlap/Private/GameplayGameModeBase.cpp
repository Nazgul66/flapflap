// Fill out your copyright notice in the Description page of Project Settings.

#include "FlapFlap.h"
#include "GameplayGameModeBase.h"
#include "GameplayPlayerController.h"
#include "GameplayPawn.h"
#include "GameplayHUD.h"

AGameplayGameModeBase::AGameplayGameModeBase()
{
	HUDClass = AGameplayHUD::StaticClass();
	PlayerControllerClass = AGameplayPlayerController::StaticClass();
	DefaultPawnClass = AGameplayPawn::StaticClass();
}


