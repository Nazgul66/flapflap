// Fill out your copyright notice in the Description page of Project Settings.

#include "FlapFlap.h"
#include "GameplayPawn.h"


// Sets default values
AGameplayPawn::AGameplayPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGameplayPawn::BeginPlay()
{
	Super::BeginPlay();

	for (TObjectIterator<AGameplayPlayerController> Player; Player; ++Player)
		this->GameplayPlayerController = *Player;
	
}

// Called every frame
void AGameplayPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AGameplayPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAction("LeftMouseButtonDown", IE_Released, this, &AGameplayPawn::MouseLeftButtonHandle);
}

void AGameplayPawn::MouseLeftButtonHandle()
{
	if (GEngine->IsValidLowLevel()) {
		GameplayPlayerController->Turn();
	}
}

