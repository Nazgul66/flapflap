// Fill out your copyright notice in the Description page of Project Settings.

#include "FlapFlap.h"
#include "Gamester.h"

Gamester::Gamester(FString Name, EPlayerSymbol Symbol, int Score)
	:Name(Name), Type(Symbol), Score(Score)
{
}

Gamester::~Gamester()
{
}

void Gamester::AdmitWin() {
	++Score;
}

EPlayerSymbol Gamester::GetSymbol() const
{
	return Type;
}

FString Gamester::GetPlayerName() const
{
	return Name;
}

int Gamester::GetScore()const
{
	return Score;
}
