// Fill out your copyright notice in the Description page of Project Settings.

#include "FlapFlap.h"
#include "CubeFieldActor.h"


// Sets default values
ACubeFieldActor::ACubeFieldActor()
	:FieldGame(EPlayerSymbol::E_CROSS, -1, -1)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	RootComponent = BoxComponent;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BoxMesh"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube"));
	if (SphereVisualAsset.Succeeded())
	{
		MeshComponent->SetStaticMesh(SphereVisualAsset.Object);

		MeshComponent->SetupAttachment(BoxComponent);
		MeshComponent->SetRelativeLocation(FVector(.0f, .0f, -50.f));
		MeshComponent->SetMobility(EComponentMobility::Movable);
		MeshComponent->SetSimulatePhysics(false);
	}
}

void ACubeFieldActor::Destroy()
{
	K2_DestroyActor();
	GetWorld()->ForceGarbageCollection(true);
	ConditionalBeginDestroy();
}

// Called when the game starts or when spawned
void ACubeFieldActor::BeginPlay()
{
	Super::BeginPlay();
}
