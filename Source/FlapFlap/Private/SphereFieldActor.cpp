// Fill out your copyright notice in the Description page of Project Settings.

#include "FlapFlap.h"
#include "SphereFieldActor.h"


// Sets default values
ASphereFieldActor::ASphereFieldActor()
	:FieldGame(EPlayerSymbol::E_CIRCLE, -1, -1)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	SphereComponent->InitSphereRadius(51.0f);

	RootComponent = SphereComponent;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SphereMesh"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));
	if (SphereVisualAsset.Succeeded())
	{
		MeshComponent->SetStaticMesh(SphereVisualAsset.Object);

		MeshComponent->SetupAttachment(SphereComponent);
		MeshComponent->SetRelativeLocation(FVector(.0f, .0f, -50.f));
		MeshComponent->SetMobility(EComponentMobility::Movable);
		MeshComponent->SetSimulatePhysics(false);
	}
}

void ASphereFieldActor::Destroy()
{
	K2_DestroyActor();
	GetWorld()->ForceGarbageCollection(true);
	ConditionalBeginDestroy();
}

// Called when the game starts or when spawned
void ASphereFieldActor::BeginPlay()
{
	Super::BeginPlay();

}

