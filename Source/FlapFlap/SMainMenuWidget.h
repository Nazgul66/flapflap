// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "SlateBasics.h"
#include "MainMenuHUD.h"
#include "Widgets/SCompoundWidget.h"

/**
 * 
 */
class FLAPFLAP_API SMainMenuWidget : public SCompoundWidget
{
public:
	TWeakObjectPtr<AMainMenuHUD> MainMenuHUD;

	SLATE_BEGIN_ARGS(SMainMenuWidget)
	{}
	SLATE_ARGUMENT(TWeakObjectPtr<AMainMenuHUD>, MainMenuHUD)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	FReply Play();
	FReply QuitGame();
};
