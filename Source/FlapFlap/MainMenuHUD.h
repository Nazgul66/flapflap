// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameFramework/HUD.h"
#include "MainMenuHUD.generated.h"

/**
 * 
 */
UCLASS()
class FLAPFLAP_API AMainMenuHUD : public AHUD
{
	GENERATED_BODY()

		virtual void PostInitializeComponents() override;
	
		TSharedPtr<class SMainMenuWidget> MainMenuUI;
	
};
