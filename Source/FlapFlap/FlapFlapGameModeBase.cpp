// Fill out your copyright notice in the Description page of Project Settings.
#include "FlapFlap.h"
#include "FlapFlapGameModeBase.h"
#include "MainMenuHUD.h"
#include "MainMenuPlayerController.h"

AFlapFlapGameModeBase::AFlapFlapGameModeBase()
{
	HUDClass = AMainMenuHUD::StaticClass();
	PlayerControllerClass = AMainMenuPlayerController::StaticClass();
}

