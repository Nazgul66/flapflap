// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "FieldGame.h"
#include "Gamester.h"
#include "GameFramework/Actor.h"
#include "FieldActor.generated.h"



UCLASS()
class FLAPFLAP_API AFieldActor : public AActor
{
	GENERATED_BODY()

public:
	AFieldActor();

	bool SpawnField(EPlayerSymbol type, FieldGame* fields[3][3]);

	UFUNCTION()
		bool CheckMouseHit(const FVector& mouse);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	// Called every frame
	void SpawnCircle(FieldGame* fields[3][3]);

	void SpawnCross(FieldGame* fields[3][3]);


	UPROPERTY()
		UBoxComponent* BoxComponent;
};
