// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "FlapFlapGameModeBase.generated.h"

/**
 *
 */
UCLASS()
class FLAPFLAP_API AFlapFlapGameModeBase: public AGameModeBase
{
	GENERATED_BODY()
public:
	AFlapFlapGameModeBase();

};
