// Fill out your copyright notice in the Description page of Project Settings.

#include "FlapFlap.h"
#include "FieldActor.h"
#include "SphereFieldActor.h"
#include "CubeFieldActor.h"

// Sets default values
AFieldActor::AFieldActor()
{
	PrimaryActorTick.bCanEverTick = false;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("SceneRoot"));
	RootComponent = BoxComponent;
}

// Called when the game starts or when spawned
void AFieldActor::BeginPlay()
{
	Super::BeginPlay();
}

void AFieldActor::SpawnCircle(FieldGame* fields[3][3])
{
	FVector location = GetActorLocation();
	FRotator rotaion = GetActorRotation();

	ASphereFieldActor* sphere = (ASphereFieldActor*)GetWorld()->SpawnActor(ASphereFieldActor::StaticClass(), &location, &rotaion);

	int lenght = GetName().Len();

	uint16_t y = FCString::Atoi(&GetName()[lenght - 1]);
	uint16_t x = FCString::Atoi(&GetName()[lenght - 3]);

	sphere->SetIndexField(x, y);

	fields[y][x] = sphere;
}

void AFieldActor::SpawnCross(FieldGame* fields[3][3])
{
	FVector location = GetActorLocation();
	FRotator rotaion = GetActorRotation();

	ACubeFieldActor* cube = (ACubeFieldActor*)GetWorld()->SpawnActor(ACubeFieldActor::StaticClass(), &location, &rotaion);

	int lenght = GetName().Len();

	uint16_t y = FCString::Atoi(&GetName()[lenght - 1]);
	uint16_t x = FCString::Atoi(&GetName()[lenght - 3]);

	cube->SetIndexField(x, y);

	fields[y][x] = cube;
}


bool AFieldActor::CheckMouseHit(const FVector & mouse)
{
	if (GEngine->IsValidLowLevel()) {
		FVector origin;
		FVector bound;
		GetActorBounds(false, origin, bound);


		//by up - down
		if (mouse.Z >= origin.Z - bound.Z  && mouse.Z <= origin.Z + bound.Z) {
			//by left - right
			if (mouse.Y >= origin.Y - bound.Y && mouse.Y <= origin.Y + bound.Y) {
				GEngine->AddOnScreenDebugMessage(-1, .016f, FColor::White, GetName());
				return true;
			}
		}
	}
	return false;
}

bool AFieldActor::SpawnField(EPlayerSymbol type, FieldGame* fields[3][3]) {

	switch (type)
	{
	case EPlayerSymbol::E_CIRCLE:
		SpawnCircle(fields);
		break;
	case EPlayerSymbol::E_CROSS:
		SpawnCross(fields);
		break;
	}

	Destroy();
	return true;
}

